document.getElementById('registerForm').addEventListener('submit', async function(event) {
    event.preventDefault();
    const firstname = document.getElementById('firstname').value;
    const lastname = document.getElementById('lastname').value;
    const country_id = document.getElementById('country').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    const requestBody = JSON.stringify({ firstname, lastname, country_id, email, password });

    try {
        const response = await fetch(base_url + 'auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: requestBody
        });
        
        if (response.status === 201) {
            window.location.href = 'login.html';
        } 

        if (response.status === 409) {
            document.getElementById('errorMessage').innerText = 'El usuario ya existe';
            document.getElementById('errorMessage').classList.remove('d-none');
        }

    } catch (error) {
        console.error('Error:', error);
        document.getElementById('errorMessage').innerText = 'Error Inesperado';
        document.getElementById('errorMessage').classList.remove('d-none');
    }
});


document.addEventListener('DOMContentLoaded', async function() {
    try {
        const response = await fetch(base_url + 'countries');
        const countries = await response.json();

        const countrySelect = document.getElementById('country');

        countries.forEach(country => {
            const option = document.createElement('option');
            option.value = country.id;
            option.textContent = country.name;
            countrySelect.appendChild(option);
        });
    } catch (error) {
        console.error('Error fetching countries:', error);
    }
});
