document.addEventListener('DOMContentLoaded', async function() {
  try {
    let token = localStorage.getItem('session');

    if (!token) {
      throw new Error('Token no encontrado en el localStorage');
    }

    let requestOptions = {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${token}`
      }
    };

    let response = await fetch(base_url + 'dashboard/profile', requestOptions);

    if (!response.ok) {
      throw new Error('Error al obtener el perfil del usuario');
    }

    let data = await response.json();

    document.getElementById('fullname').innerHTML = `${data.user.firstname} ${data.user.lastname}`;
    document.getElementById('temperature').innerHTML = `${data.temperature}`;

    response = await fetch(base_url + 'dashboard/tasks', requestOptions);

    if (!response.ok) {
      throw new Error('Error al obtener las tareas');
    }

    const tasks = await response.json();

    const tasksContainer = document.getElementById('tasksContainer');
    tasks.forEach(task => {
      const card = document.createElement('div');
      card.classList.add('card', 'mb-3', 'col-md-4', 'px-2');

      const cardBody = document.createElement('div');
      cardBody.classList.add('card-body');

      const title = document.createElement('h5');
      title.classList.add('card-title');
      title.textContent = task.title;

      const description = document.createElement('p');
      description.classList.add('card-text');
      description.textContent = task.description;

      const verMasButton = document.createElement('button');
      verMasButton.classList.add('btn', 'btn-primary', 'btn-sm');
      verMasButton.textContent = 'Ver más';
      verMasButton.addEventListener('click', () => {
          document.getElementById('modalTitle').textContent = task.title;
          document.getElementById('modalDescription').textContent = task.description;

          // Limpiar contenido anterior de la tabla
          const propertiesTableBody = document.getElementById('propertiesTableBody');
          propertiesTableBody.innerHTML = '';

          // Agregar cada propiedad a la tabla
          task.properties.forEach(property => {
              const row = propertiesTableBody.insertRow();
              const descriptionCell = row.insertCell(0);
              const valueCell = row.insertCell(1);
              descriptionCell.textContent = property.description;
              valueCell.textContent = property.value;
          });

          $('#taskModal').modal('show');
      });


      cardBody.appendChild(title);
      cardBody.appendChild(description);
      cardBody.appendChild(verMasButton);
      card.appendChild(cardBody);

      tasksContainer.appendChild(card);
    });

    console.log(data);
  } catch (error) {
    console.error(error);
    window.location = 'login.html'
  }
});


document.getElementById('addPropertyBtn').addEventListener('click', function() {
  const propertiesSection = document.getElementById('propertiesSection');
  
  const propertyRow = document.createElement('div');
  propertyRow.classList.add('form-row', 'mb-3');
  
  const descriptionCol = document.createElement('div');
  descriptionCol.classList.add('col');
  const descriptionInput = document.createElement('input');
  descriptionInput.type = 'text';
  descriptionInput.classList.add('form-control');
  descriptionInput.placeholder = 'Descripción';
  descriptionInput.name = 'propertyDescription[]';
  descriptionInput.required = true;
  descriptionCol.appendChild(descriptionInput);
  
  const valueCol = document.createElement('div');
  valueCol.classList.add('col');
  const valueInput = document.createElement('input');
  valueInput.type = 'text';
  valueInput.classList.add('form-control');
  valueInput.placeholder = 'Valor';
  valueInput.name = 'propertyValue[]';
  valueInput.required = true;
  valueCol.appendChild(valueInput);
  
  const deleteBtnCol = document.createElement('div');
  deleteBtnCol.classList.add('col-auto');
  const deleteBtn = document.createElement('button');
  deleteBtn.type = 'button';
  deleteBtn.classList.add('btn', 'btn-danger');
  deleteBtn.textContent = '-';
  deleteBtn.addEventListener('click', function() {
    propertiesSection.removeChild(propertyRow);
  });
  deleteBtnCol.appendChild(deleteBtn);
  
  propertyRow.appendChild(descriptionCol);
  propertyRow.appendChild(valueCol);
  propertyRow.appendChild(deleteBtnCol);
  
  propertiesSection.appendChild(propertyRow);
});



async function addTask(formData) {
  try {
    const token = localStorage.getItem('session');

    if (!token) {
      throw new Error('Token no encontrado en el localStorage');
    }

    const requestOptions = {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    };

    const response = await fetch(base_url + 'dashboard/task', requestOptions);

    if (!response.ok) {
      throw new Error('Error al agregar la tarea');
    }

    const data = await response.json();
    console.log(data); 

    return data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}


document.addEventListener('DOMContentLoaded', function() {
  // Escuchamos el evento de submit del formulario
  document.getElementById('addTaskForm').addEventListener('submit', async function(event) {
    event.preventDefault(); // Evitamos que se recargue la página al enviar el formulario

    try {
      // Llamamos a la función para agregar la tarea
      await addTask();
      window.location.reload()
      this.reset();
    } catch (error) {
      console.error(error);
      // Aquí puedes manejar el error, por ejemplo, mostrando un mensaje al usuario
    }
  });
});

// Función para enviar los datos del formulario y agregar una nueva tarea
async function addTask() {
  try {
    const token = localStorage.getItem('session');

    if (!token) {
      throw new Error('Token no encontrado en el localStorage');
    }

    // Capturamos los datos del formulario
    const formData = {
      title: document.getElementById('title').value,
      description: document.getElementById('description').value,
      properties: [] // Aquí puedes capturar las propiedades si es necesario
    };

    // Capturar los datos de las propiedades adicionales
    const propertyRows = document.querySelectorAll('.form-row');
    propertyRows.forEach(row => {
      const descriptionInput = row.querySelector('input[name="propertyDescription[]"]');
      const valueInput = row.querySelector('input[name="propertyValue[]"]');
      if(descriptionInput && valueInput){
        const property = {
          description: descriptionInput.value,
          value: valueInput.value
        };
        formData.properties.push(property);  
      }
    });

    // Construimos la solicitud POST con los datos del formulario
    const requestOptions = {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(formData)
    };

    console.log(formData)

    // Enviamos la solicitud POST a la ruta "dashboard/task"
    const response = await fetch(base_url + 'dashboard/tasks', requestOptions);

    // Verificamos si la solicitud fue exitosa
    if (!response.ok) {
      throw new Error('Error al agregar la tarea');
    }

    // Si la solicitud fue exitosa, procesamos los datos de la respuesta (opcional)
    const data = await response.json();
    console.log(data); // Aquí puedes manejar la respuesta si es necesario

    return data; // Opcional: devuelve los datos de la respuesta
  } catch (error) {
    console.error(error);
    throw error; // Lanza el error para que pueda ser manejado externamente
  }
}


  document.getElementById('logoutLink').addEventListener('click', async function(event) {
  event.preventDefault(); // Evitamos que se ejecute el enlace
  
  try {
    const token = localStorage.getItem('session');

    if (!token) {
      throw new Error('Token no encontrado en el localStorage');
    }

    const requestOptions = {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    };

    const response = await fetch(base_url + 'auth/logout', requestOptions);

    if (!response.ok) {
      throw new Error('Error al cerrar sesión');
    }

    // Eliminamos la sesión del localStorage
    localStorage.removeItem('session');
    
    // Redireccionamos a la página de login
    window.location.href = 'login.html';
  } catch (error) {
    console.error(error);
    // Aquí puedes manejar el error, por ejemplo, mostrando un mensaje al usuario
  }
});
