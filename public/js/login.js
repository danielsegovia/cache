document.getElementById('loginForm').addEventListener('submit', async function(event) {
    event.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const requestBody = JSON.stringify({ email, password });

    try {
        const response = await fetch(base_url + 'auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: requestBody
        });
        
        if (response.status === 201) {
            const data = await response.json();
            localStorage.setItem('session', data.token);
            window.location.href = 'index.html';
            return true;
        }
        
        document.getElementById('errorMessage').innerText = 'Usuario o contraseña incorrectos';
        document.getElementById('errorMessage').classList.remove('d-none');

    } catch (error) {
        console.error('Error:', error);
        document.getElementById('errorMessage').innerText = 'Error Inesperado';
        document.getElementById('errorMessage').classList.remove('d-none');
    }
});
