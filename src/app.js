const api_version = 'v1'
const express = require('express');
require('dotenv').config();
const { validateToken } = require('./middlewares/validateToken');
const cors = require('cors')

const app = express();

app.use(cors())
app.use(express.json());

// Servir archivos estáticos desde la carpeta 'public'
app.use(express.static('public'));

app.get('/ping', (req, res) => { return res.status(200).json({ msj: "app CACHE is live" }) })

// Routes
const dashboardRouter = require('./routes/dashboard');
app.use(`/${api_version}/dashboard`, dashboardRouter);

const authRouter = require('./routes/auth');
app.use(`/${api_version}/auth`, authRouter);

const countriesRouter = require('./routes/countries');
app.use(`/${api_version}/countries`, countriesRouter);


//caotura de errorescc
app.use(function onError(err, req, res, next) {
    res.status(err.code || 500).json({ error: err.message });
});

module.exports = app