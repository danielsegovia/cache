const jwt = require('jsonwebtoken');
require('dotenv').config();
const memcached = require('../config/memcached');

exports.verificarToken = async (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(401).json({ success: false, message: 'Token no proporcionado' });
    }

    if (!token.startsWith('Bearer ')) {
        return res.status(401).json({ success: false, message: 'Formato de token incorrecto' });
    }

    const tokenString = token.split(' ')[1];

    try {
        const decoded = jwt.verify(tokenString, process.env.JWT_SECRET);

        const cache_key = `user_token_${decoded.user.id}`
        let token = await memcached.get(cache_key);

        //comparo el token enviado por el usuario con el guardado en mi cache para ese usuario
        if(token !== tokenString){
            console.log('Token invalidado')
            return res.status(401).json({ success: false, message: 'El token ha sido invalidado' });
        }

        req.user = decoded.user;

        // Pasar al siguiente middleware
        next();
    } catch (error) {
        console.error(error);
        return res.status(401).json({ success: false, message: 'Token inválido' });
    }
};
