const Joi = require('joi');

const schemaPostTasks = Joi.object({
    title: Joi.string().required(),
    description: Joi.string().required()
  });

  

exports.validatePostTasks = (req, res, next) => {
    const { title, description } = req.body;
    const { error } = schemaPostTasks.validate({ title, description });

  if (error) {
    return res.status(400).send(error.details[0].message);
  } else {
    next();
  }
}