const Joi = require('joi');

const schemaRegister = Joi.object({
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    email: Joi.string().email().required(),
    password: Joi.string().required(),
  });

  

exports.validateRegister = (req, res, next) => {
    const { firstname, lastname, email, password } = req.body;
    const { error } = schemaRegister.validate({ firstname, lastname, email, password });

  if (error) {
    return res.status(400).send(error.details[0].message);
  } else {
    next();
  }
}

const schemaLogin = Joi.object({
    password: Joi.string().required(),
    email: Joi.string().email().required(),
});

  

exports.validateLogin = (req, res, next) => {
    const { email, password } = req.body;
    const { error } = schemaLogin.validate({ email, password });

  if (error) {
    return res.status(400).send(error.details[0].message);
  } else {
    next();
  }
}