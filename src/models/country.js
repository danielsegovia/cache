'use strict';

module.exports = (sequelize, DataTypes) => {
  const Country = sequelize.define('Country', {
    name: DataTypes.STRING,
  }, {
    tableName: "countries",
    timestamps: false
  });

  Country.associate = function (models) {
    Country.hasMany(models.User, { foreignKey: 'country_id' });
  };

  return Country;
};