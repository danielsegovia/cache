'use strict';

const Sequelize = require('sequelize');
const config = require('../config/database')[process.env.NODE_ENV || 'development'];

const sequelize = new Sequelize(config.database, config.username, config.password, config);

const User = require('./User')(sequelize, Sequelize.DataTypes);
const Country = require('./Country')(sequelize, Sequelize.DataTypes);

User.belongsTo(Country, { foreignKey: 'country_id' });

const db = {
  User,
  Country,
};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;