const mongoose = require('../config/mongo')
const memcached = require('../config/memcached');

// Definir el esquema de la tarea
const taskSchema = new mongoose.Schema({
  user_id: {
    type: Number,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  properties: [{
    description: {
      type: String,
      required: true
    },
    value: {
      type: String,
      required: true
    }
  }],
  createdAt: {
    type: Date,
    default: Date.now
  }
});



const originalExec = mongoose.Query.prototype.exec;
mongoose.Query.prototype.exec = async function() {

  const filters = this.getQuery();
  let cache_key;

  console.log('============= Tareas ===============');

  if (filters.user_id !== undefined) {
    cache_key = `user_tasks_${filters.user_id}`
    const cached_result = await memcached.get(cache_key);
    if (cached_result) {
      console.log('Tareas retornadas desde cache con la key:', cache_key);
      return cached_result;
    }
  }

  const result = await originalExec.apply(this, arguments);

  if(cache_key){
    console.log('Tareas guardadas en cache con la key:', cache_key);
    await memcached.set(cache_key, result, 3600);
  }
  return result
}

taskSchema.post('save', async function(doc) {
  try {
    if (doc && doc.user_id !== undefined) {
      const cache_key = `user_tasks_${doc.user_id}`;
      await memcached.del(cache_key);
      console.log(`Clave de caché eliminada: ${cache_key}`);
    } 
  } catch (error) {
    console.error('Error al eliminar la clave de caché:', error);
  }
});

const Task = mongoose.model('Task', taskSchema);

module.exports = Task;
