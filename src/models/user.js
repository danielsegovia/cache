'use strict';
require('dotenv').config();
const bcrypt = require('bcrypt');
const salt = parseInt(process.env.SALT_ROUNDS)
const memcached = require('../config/memcached');

module.exports = (sequelize, DataTypes) => {

  const User = sequelize.define('User', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    password: DataTypes.STRING,
    country_id: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
  }, {
    tableName: 'users',
    timestamps: false,
    hooks: {
      beforeCreate: async (user) => {
        const hashedPassword = await bcrypt.hash(user.password, salt);
        user.password = hashedPassword;
      },
      beforeFind: async (options) => {
        console.log('============= User Data ===============');
        if (options.where && options.where.id) {
          const cache_key = `user_${options.where.id}`;
          const cached_result = await memcached.get(cache_key);

          if (cached_result) {
            console.log('User data retornada desde cache con la key:', cache_key);
            options.cacheHit = true;
            options.instance = cached_result;
          }
        }
      },
      afterFind: async (result, options) => {
        if (result && options.where && options.where.id && !options.cacheHit) {
          const cache_key = `user_${options.where.id}`;
          console.log('User data guardada en cache con la key:', cache_key);
          await memcached.set(cache_key, result, 3600);
        }
      }
  },

  });

  User.associate = function (models) {
    User.belongsTo(models.Country, { foreignKey: 'country_id' });
  };

  return User;
};