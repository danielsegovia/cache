const axios = require('axios');
const memcached = require('../config/memcached');

const axiosInstance = axios.create({
  baseURL: 'http://api.openweathermap.org/data/2.5',
  timeout: 5000
});

axiosInstance.interceptors.request.use(async (config) => {

  console.log('============= Clima ===============');

  const cache_key = `${config.method}_${config.url}_${config.params.q}`;

  const cachedResponse = await memcached.get(cache_key);

  if (cachedResponse) {
    console.log('Temperatura retornada desde cache con la key:', cache_key);
    config.cachedResponse = cachedResponse;
    return Promise.reject({ isCached: true, cachedResponse });
  }

  return config;
}, (error) => Promise.reject(error));

axiosInstance.interceptors.response.use(async (response) => {
  const cache_key = `${response.config.method}_${response.config.url}_${response.config.params.q}`;

  console.log('Temperatura guardada en cache con la key:', cache_key);
  await memcached.set(cache_key, response.data, 3600);

  return response;
}, (error) => {
  if (error.isCached) {
    return Promise.resolve({ data: error.cachedResponse });
  }

  return Promise.reject(error);
});

module.exports = axiosInstance;
