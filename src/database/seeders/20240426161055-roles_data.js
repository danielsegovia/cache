'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    let countries = [
      { name: 'Argentina' },
      { name: 'Bolivia' },
      { name: 'Brasil' },
      { name: 'Chile' },
      { name: 'Colombia' },
      { name: 'Costa Rica' },
      { name: 'Cuba' },
      { name: 'Ecuador' },
      { name: 'El Salvador' },
      { name: 'Guatemala' },
      { name: 'Honduras' },
      { name: 'México' },
      { name: 'Nicaragua' },
      { name: 'Panamá' },
      { name: 'Paraguay' },
      { name: 'Perú' },
      { name: 'República Dominicana' },
      { name: 'Uruguay' },
      { name: 'Venezuela' }
    ];

    await queryInterface.bulkInsert('countries', countries, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete('countries', null, {});
  }
};
