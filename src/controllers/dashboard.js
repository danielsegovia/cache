require('dotenv').config();
const db = require('../models'); 
const Task = require('../models/Task');
const axios = require('../service/weather');

exports.getProfile = async (req, res) => {
    try {


        const user = await db.User.findByPk(req.user.id, {
          attributes: ['id', 'firstname', 'lastname', 'email']
        });

        const response = await axios.get(`/weather`, {
          params: {
            q: req.user.country_name,
            appid: process.env.OPENWEATHERMAP_TOKEN,
            units: 'metric'
          }
        });

        const temperature = Math.ceil(response.data.main.temp);

        return res.status(200).json({user, temperature: `${req.user.country_name} ${temperature}º`});

    } catch (error) {
        console.error(error);
        return res.status(500).json({ error: 'Error al obtener la temperatura de Buenos Aires' });
    }
};

exports.getTask = async (req, res) => {
    try {
        return res.status(200).json({ msj: "ok" });
    } catch (error) {
        throw new Error(error)
    }
}

exports.getTasks = async (req, res) => {
    try {
        const tasks = await Task.find({ user_id: req.user.id }).sort({ createdAt: -1 });

        return res.status(200).json(tasks);
    } catch (error) {
        throw new Error(error)
    }
}

exports.postTask = async (req, res) => {
    try {
        
        const { title, description, properties } = req.body;

        const newTask = new Task({
          user_id: req.user.id,
          title,
          description,
          properties
        });

        const savedTask = await newTask.save();
        return res.status(200).json({ msj: "ok" });
    } catch (error) {
        console.log(error)
        return res.status(500).json({ error});
    }
}