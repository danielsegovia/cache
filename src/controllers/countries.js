const {
    Country,
} = require('../models/index');

exports.getCountries = async (req, res) => {
    try {

        const countries = await Country.findAll();
        res.status(201).json(countries);

    } catch (error) {
        console.error(error)
        return res.status(500).json({ msj: "error inesperado", error: error.message });
    }
}