const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config();
const memcached = require('../config/memcached');

const {
    User,
    Country
} = require('../models/index');

const {ROL_GUEST} = require('../config/constants');

exports.register = async (req, res) => {
    try {

        const userExists = await User.findOne({
            where: {
              email: req.body.email
            }
        });

        if (userExists) {
            return res.status(409).json({ message: 'ok' });
        }

        const newUser = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            country_id: req.body.country_id,
            email: req.body.email,
            password: req.body.password,
            rol_id: ROL_GUEST
        }

        await User.create(newUser);

        res.status(201).json({ message: 'ok' });
    } catch (error) {
        console.error(error)
        return res.status(500).json({ msj: "error inesperado", error: error.message });
    }
}

exports.login = async (req, res) => {
    try {

        const {email, password} = req.body;
        const user = await User.findOne({ 
          where: { email },
          include: { model: Country }
        });

        if (!user) return res.status(401).json({ success: false, message: 'Usuario o contraseña incorrecta.' });

        const passwordMatch = await bcrypt.compare(password, user.password);

        if (!passwordMatch) return res.status(401).json({ success: false, message: 'Usuario o contraseña incorrecta.' });

        const token = jwt.sign({user: {firstname: user.firstname, lastname: user.lastname, id: user.id, country_id: user.Country.id, country_name: user.Country.name}}, process.env.JWT_SECRET, { expiresIn: '1h' });
        const cache_key = `user_token_${user.id}`
        console.log(`token almacenado en cache para user id ${user.id}`)
        await memcached.set(cache_key, token, 3600);

        res.status(201).json({ message: 'ok', token });
    } catch (error) {
        console.error(error)
        return res.status(500).json({ msj: "error inesperado", error: error.message });
    }
}


exports.logout = async (req, res) => {
    try {
        const cache_key = `user_token_${req.user.id}`
        await memcached.del(cache_key)
        console.log(`el token para user_${req.user.id} ha sido destruida`)
        res.status(200).json({ message: 'ok' });
    } catch (error) {
        console.error(error)
        return res.status(500).json({ msj: "error inesperado", error: error.message });
    }
}