const { Ticket } = require('../models/index');

async function getTicketById(ticketId) {
    try {
        const ticketToAssign = await Ticket.findOne({ where: { id: ticketId } });
        if (!ticketToAssign) {
            throw new Error('No se encontró el ticket con el id proporcionado');
        }
        return ticketToAssign;

    } catch (error) {
        console.error(error);
        throw error;
    }
}

module.exports = {
    getTicketById
};