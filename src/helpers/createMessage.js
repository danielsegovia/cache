const { emailConfig } = require('../config/mailing')

function getEventConfig(eventTypeName) {
    try {
        return emailConfig[eventTypeName];
    } catch (error) {
        console.error(error);
    }
}

function getSubject(user, eventTypeName, template, language) {
    try {
        // Accede a la configuración del evento
        const eventConfig = emailConfig[eventTypeName];

        // Accede al objeto de 'subjects' para el evento
        const subjects = eventConfig.subjects;

        // Accede al 'subject' específico para el template y el idioma
        let subjectTemplate = subjects[template][language];

        // Si el subject es una función, llámala con los argumentos necesarios
        if (typeof subjectTemplate === 'function') {
            subjectTemplate = subjectTemplate(user, eventConfig.eventName);
        }

        return subjectTemplate;
    } catch (error) {
        console.error(error);
    }
}

async function createMessage(user, event_type_id, template) {
    try {
        let userInfo = { 
            id: user.id, 
            email: user.email, 
            hash: user.hash, 
            firstname: user.firstname, 
            lastname: user.lastname, 
            lastname_mother: user.lastname_mother,
            company_name: user.company_name,
            lang: user.language || 'esp'
        };
        
        let eventTypeName;
        switch (event_type_id) {
            case 1:
                eventTypeName = 'tlw';
                break;
            case 2:
                eventTypeName = 'tft';
                break;
            default:
                throw new Error('Invalid event_type_id');
        }

        const eventConfig = getEventConfig(eventTypeName);
        const subject = getSubject(user, eventTypeName, template, userInfo.lang);

        return {
            user: userInfo,
            event: eventTypeName,
            template: template,
            lang: userInfo.lang,
            sender: {
                subject: subject,
                from_email: eventConfig.sender.from_email,
                from_name: eventConfig.sender.from_name
            }
        };

    } catch (error) {
        console.error(error);
    }
}

module.exports = {
    createMessage
};