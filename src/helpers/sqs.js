const { SendMessageCommand } = require('@aws-sdk/client-sqs');
const { sqs, queues } = require('../config/sqs');

// Función para enviar mensajes a la cola SQS
async function sendMessageToQueue(message, queue) {
  //validar que message.user sea obligatorio
  //validar que queues.queue exista

  try {
    let timestamp = Date.now();
    const params = {
      MessageBody: JSON.stringify(message),
      QueueUrl: queues[queue],
      MessageGroupId: `${queue}-user-${message.user.id}-${timestamp}`,
    };

    const command = new SendMessageCommand(params);
    const resultado = await sqs.send(command);
    console.log(resultado)
    
    return resultado.MessageId;
  } catch (error) {
    console.error('SQS error:', error);
    throw error;
  }
}

module.exports = {
  sendMessageToQueue,
};