const { Event } = require('../models/index');

async function getActiveEvent(event_type_id) {
    try {
        const currentEventId = await Event.findOne({ where: { event_type_id: event_type_id, active: true }, attributes: ['id'] });
        if (!currentEventId) {
            throw new Error('No se encontró un evento activo para este tipo de evento');
        }
        return currentEventId;

    } catch (error) {
        console.error(error);
        throw error;
    }
}

module.exports = {
    getActiveEvent,
};