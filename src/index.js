const app = require('./app');
require('dotenv').config();
const app_port = process.env.APP_PORT || process.exit(1);
const { sequelize } = require('./models/index');


try {
    app.listen(app_port, () => {
    	
        console.log(`Servidor en funcionamiento en el puerto ${app_port}`);
        const options = {}
        //options.force = true

        sequelize.sync(options).then(() => {
            console.log('Se ha establecido la conexión al mysql');
        })
        
    });
} catch (error) {
    console.error(`Error al iniciar el servidor: ${error.message}`);
    process.exit(2);
}