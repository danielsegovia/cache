const express = require('express');
const dashboardRouter = express.Router();
const { 
	getProfile,
	getTask,
    getTasks,
    postTask,
} = require('../controllers/dashboard');

const { 
	verificarToken,
} = require('../middlewares/validateToken');

const { 
	validatePostTasks,
} = require('../middlewares/validateDashboard');


dashboardRouter.use(verificarToken)

dashboardRouter.get('/tasks', getTasks);
dashboardRouter.get('/profile', getProfile);
dashboardRouter.post('/tasks', validatePostTasks, postTask);

module.exports = dashboardRouter;