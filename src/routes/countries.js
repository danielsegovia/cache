const express = require('express');
const authRouter = express.Router();


const { 
	getCountries,
} = require('../controllers/countries');


authRouter.get('/', getCountries);

module.exports = authRouter;