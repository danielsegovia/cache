const express = require('express');
const authRouter = express.Router();


const { 
	register,
    login,
    logout,
} = require('../controllers/auth');

const { 
	validateRegister,
	validateLogin
} = require('../middlewares/validateAuth');

const { 
	verificarToken,
} = require('../middlewares/validateToken');



authRouter.post('/register', validateRegister, register);
authRouter.post('/login', validateLogin, login);
authRouter.delete('/logout', verificarToken, logout);

module.exports = authRouter;