const Memcached = require('memcached-promisify');
const memcached = new Memcached(process.env.MENCACHED_URL);

module.exports = memcached;