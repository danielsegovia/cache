require('dotenv').config();

module.exports = {
  development: {
        username: process.env.DB_USERNAME || 'root',
        password: process.env.DB_PASSWORD || "",
        database: process.env.DB_NAME,
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT || 'mysql',
        logging: false,
        // Seeds config
        seederStorage: "sequelize",
        seederStorageTableName: "seeds",
        //Migrations config
        migrationStorage: "sequelize",
        migrationStorageTableName: "migrations",

        define: {
          underscored: true,
        }
      },
      test: {
        username: process.env.DB_USERNAME || 'root',
        password: process.env.DB_PASSWORD || "",
        database: "core_testing",
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT || 'mysql',
        logging: false,
        // Seeds config
        seederStorage: "sequelize",
        seederStorageTableName: "seeds",
        //Migrations config
        migrationStorage: "sequelize",
        migrationStorageTableName: "migrations",

        define: {
          underscored: true,
        }
      },
      production: {
        username: process.env.DB_USERNAME || 'root',
        password: process.env.DB_PASSWORD || "",
        database: process.env.DB_NAME,
        host: process.env.DB_HOST || 'localhost',
        port: process.env.DB_PORT,
        dialect: process.env.DB_DIALECT || 'mysql',
        logging: false,
        // Seeds config
        seederStorage: "sequelize",
        seederStorageTableName: "seeds",
        //Migrations config
        migrationStorage: "sequelize",
        migrationStorageTableName: "migrations",

        define: {
          underscored: true,
        }
      }
}