README.md



#crear voluem para mysql
docker volume create mysql_data_volume

#iniciar mysql
docker run -d -p 8000:3306 -e MYSQL_ALLOW_EMPTY_PASSWORD=true -v mysql_data_volume:/var/lib/mysql --name mysql_container mysql:latest

#crear voluem para mongo
docker volume create mongodb_data
#iniciar mongo
docker run -d -p 8001:27017 --name mongodb_instance -v mongodb_data:/data/db mongo

#iniciar memcached
docker run -d --name memcached -p 11211:11211 memcached
